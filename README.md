# Mixed Laplace Eigensolver

**Author**: Luca Heltai <luca.heltai@sissa.it>

This code contains some experiments based on the article

*Optimal convergence of adaptive FEM for eigenvalue clusters in mixed form*
Daniele Boffi, Dietmar Gallistl, Francesca Gardini, Lucia Gastaldi
http://arxiv.org/abs/1504.06418

It solves the Laplace Problem in mixed form, using adaptive mesh 
refinement based on an estimator constructed using eigenvectors.

## Instruction

You need both the deal.II library (http://www.dealii.org) and the deal.II ToolKit (https://github.com/mathLab/deal2lkit)

	git clone https://gitlab.com/code_projects/mixed-eigen-laplace.git
    cd mixed-eigen-laplace
    mkdir build
    cd build
    cmake -DDEAL_II_DIR=/path/to/dealii -DD2K_DIR=/path/to/deal2lkit ..
	make

## Input Parameters

When you run the program for the first time, you'll get a file like this in the running directory (with a lot more options).

The bulk parameter is called `Top fraction`. If you want to enable also coarsening, then set `Bottom fraction` to a non-zero value.

```bash
# Parameter file generated with 
# D2K_GIT_BRANCH=       eigenvalues
# D2K_GIT_SHORTREV=     12cb45d

subsection Global parameters
  set Degree of the pressure space                  = 0
  set Eigenvector indices for a posteriori estimate = 4,5
  set Estimator                                     = boffi
  set Mark strategy                                 = fraction
  set Initial global refinement                     = 3
  set Total number of cycles                        = 6
  set Number of eigenvalues to compute              = 9
  set Top fraction                                  = .35
  set Bottom fraction                               = 0.0
end


subsection Data out
  set Incremental run prefix = 
  set Output partitioning    = false
  set Problem base name      = solution
  set Solution names         = u
  subsection Solution output format
    set Output format = vtu
    set Subdivisions  = 1
  end
end

subsection ErrorHandler<1>
  set Compute error            = true
  set Error file format        = gpl
  set Output error tables      = true
  set Table names              = error
  set Write error files        = true
  subsection Table 0
    set Add convergence rates          = false
    set Extra terms                    = cells,dofs
    set List of error norms to compute = Custom
    set Rate key                       = cells
  end
end
```


## Licence

This software is subject to L-GPL version 2.0. See the file LICENCE for
more information.
