#ifndef __eigen_problem_h
#define __eigen_problem_h

#include <deal.II/grid/tria.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/grid/grid_generator.h>

#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/dofs/dof_accessor.h>

#include <deal.II/fe/fe_dgq.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/mapping_q1.h>

#include <deal.II/dofs/dof_tools.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/grid/grid_refinement.h>

#include <deal.II/fe/fe_values.h>

#include <deal.II/fe/fe_raviart_thomas.h>

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/numerics/data_out.h>

#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/compressed_sparsity_pattern.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>

#include <deal.II/base/index_set.h>
#include <deal.II/lac/petsc_sparse_matrix.h>
#include <deal.II/lac/petsc_parallel_vector.h>
#include <deal.II/lac/slepc_solver.h>
#include <deal.II/lac/slepc_spectral_transformation.h>

#include <deal.II/base/mpi.h>

#include <fstream>
#include <iostream>

#include <deal2lkit/parsed_data_out.h>
#include <deal2lkit/error_handler.h>
#include <deal2lkit/utilities.h>
#include <deal2lkit/dof_utilities.h>
#include <deal2lkit/fe_values_cache.h>


using namespace dealii;



template<int dim>
class EigenProblem : public ParameterAcceptor
{
public:
    
  EigenProblem ();
    
  virtual void declare_parameters(ParameterHandler &prm);
    
  void run ();
    
    
private:
  void make_grid_fe ();
  void setup_system ();
  void assemble_system ();
  void solve ();
  void estimate ();
  void mark_and_refine ();
  void compute_error ();
  void output_results (const unsigned int cycle);
    
  shared_ptr<Triangulation<dim> >     triangulation;
  shared_ptr<FiniteElement<dim,dim> > fe;
  shared_ptr<DoFHandler<dim> >        dof_handler;
    
  ConstraintMatrix     constraints;
  PETScWrappers::SparseMatrix             stiffness_matrix, mass_matrix;
  std::vector<PETScWrappers::MPI::Vector> eigenfunctions;
  std::vector<double>                     eigenvalues;

  Vector<float> 			  estimated_error_per_cell;
    
  ParsedDataOut<dim,dim> data_out;
    
  unsigned int n_cycles;
  unsigned int initial_refinement;
  unsigned int n_eigenvalues;
  unsigned int degree;
  std::vector<unsigned int> eigenvalue_refinement_indices;
  std::string mark_strategy;
  double top_fraction;
  double bottom_fraction;
  std::string estimator_type;
    
  ErrorHandler<1> eh;
};


#endif
