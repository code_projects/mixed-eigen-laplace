/* ---------------------------------------------------------------------
 * Author: Luca Heltai
 * ---------------------------------------------------------------------
 */
#include "eigen_problem.h"

template <int dim>
EigenProblem<dim>::EigenProblem ()
  :
  ParameterAcceptor("Global parameters"),
  data_out("Data out", "vtu")
{}


template <int dim>
void EigenProblem<dim>::declare_parameters(ParameterHandler &prm)
{
    
  add_parameter(prm, &initial_refinement, "Initial global refinement", "3",
		Patterns::Integer(0));
    
  add_parameter(prm, &n_cycles, "Total number of cycles", "6",
		Patterns::Integer(0));
    
    
  add_parameter(prm, &n_eigenvalues, "Number of eigenvalues to compute", "9",
		Patterns::Integer(0,9));

    
  add_parameter(prm, &degree, "Degree of the pressure space", "0",
		Patterns::Integer(0));
    

  add_parameter(prm, &eigenvalue_refinement_indices, "Eigenvector indices for a posteriori estimate", "3",
		Patterns::List(Patterns::Integer(0),0));

  add_parameter(prm, &mark_strategy, "Mark strategy", "fraction",
		Patterns::Selection("fraction|number|optimize"));

  
  add_parameter(prm, &top_fraction, "Top fraction", ".35",
		Patterns::Double(0.0, 1.0));

  add_parameter(prm, &bottom_fraction, "Bottom fraction", "0.0",
		Patterns::Double(0.0, 1.0));

  add_parameter(prm, &estimator_type, "Estimator", "boffi",
		Patterns::Selection("boffi|kelly"));
}

template <int dim>
void EigenProblem<dim>::make_grid_fe ()
{
  triangulation = SP(new Triangulation<dim>());
  GridGenerator::hyper_cube (*triangulation, 0, numbers::PI);

    
  triangulation->refine_global(initial_refinement);
    
  dof_handler = SP(new DoFHandler<dim>(*triangulation));
    
  std::cout << "Number of active cells: "
	    << triangulation->n_active_cells()
	    << std::endl;
    
  fe=SP(new FESystem<dim>(FE_RaviartThomas<dim>(degree), 1, FE_DGQ<dim>(degree), 1));
    
}



template <int dim>
void EigenProblem<dim>::setup_system ()
{
  dof_handler->distribute_dofs (*fe);
  DoFRenumbering::component_wise (*dof_handler);
    
  std::vector<types::global_dof_index> dofs_per_component (dim+1);
  DoFTools::count_dofs_per_component (*dof_handler, dofs_per_component);
  const unsigned int n_u = dofs_per_component[0],
    n_p = dofs_per_component[dim];
  std::cout << "Number of active cells: "
	    << triangulation->n_active_cells()
	    << std::endl
	    << "Total number of cells: "
	    << triangulation->n_cells()
	    << std::endl
	    << "Number of degrees of freedom: "
	    << dof_handler->n_dofs()
	    << " (" << n_u << '+' << n_p << ')'
	    << std::endl;
    
  constraints.clear ();
    
  DoFTools::make_hanging_node_constraints (*dof_handler, constraints);
    
  constraints.close ();

  std::cout << "Constrained degrees of freedom: "
	    << constraints.n_constraints() << std::endl;
    
  stiffness_matrix.reinit (dof_handler->n_dofs(),
			   dof_handler->n_dofs(),
			   dof_handler->max_couplings_between_dofs());
  mass_matrix.reinit (dof_handler->n_dofs(),
		      dof_handler->n_dofs(),
		      dof_handler->max_couplings_between_dofs());
    
  IndexSet eigenfunction_index_set = dof_handler->locally_owned_dofs ();
  eigenfunctions
    .resize (n_eigenvalues);
  for (unsigned int i=0; i<n_eigenvalues; ++i)
    eigenfunctions[i].reinit (eigenfunction_index_set, MPI_COMM_WORLD);
  eigenvalues.resize (eigenfunctions.size ());

  estimated_error_per_cell.reinit(triangulation->n_active_cells());
}


template <int dim>
void EigenProblem<dim>::assemble_system ()
{
  QGauss<dim>   quadrature_formula(degree+2);
  QGauss<dim-1> face_quadrature_formula(degree+2);
  FEValues<dim> fe_values (*fe, quadrature_formula,
			   update_values    | update_gradients |
			   update_quadrature_points  | update_JxW_values);
  FEFaceValues<dim> fe_face_values (*fe, face_quadrature_formula,
				    update_values    | update_normal_vectors |
				    update_quadrature_points  | update_JxW_values);
  const unsigned int   dofs_per_cell   = fe->dofs_per_cell;
  const unsigned int   n_q_points      = quadrature_formula.size();
  const unsigned int   n_face_q_points = face_quadrature_formula.size();
  FullMatrix<double>   local_matrix (dofs_per_cell, dofs_per_cell);
  FullMatrix<double>   local_mass_matrix (dofs_per_cell, dofs_per_cell);

  std::vector<types::global_dof_index> local_dof_indices (dofs_per_cell);

  const FEValuesExtractors::Vector velocities (0);
  const FEValuesExtractors::Scalar pressure (dim);
    
  typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler->begin_active(),
    endc = dof_handler->end();
  for (; cell!=endc; ++cell)
    {
      fe_values.reinit (cell);
      local_matrix = 0;
      local_mass_matrix = 0;
 
      for (unsigned int q=0; q<n_q_points; ++q)
	for (unsigned int i=0; i<dofs_per_cell; ++i)
	  {
	    const Tensor<1,dim> phi_i_u     = fe_values[velocities].value (i, q);
	    const double        div_phi_i_u = fe_values[velocities].divergence (i, q);
	    const double        phi_i_p     = fe_values[pressure].value (i, q);
	    for (unsigned int j=0; j<dofs_per_cell; ++j)
	      {
		const Tensor<1,dim> phi_j_u     = fe_values[velocities].value (j, q);
		const double        div_phi_j_u = fe_values[velocities].divergence (j, q);
		const double        phi_j_p     = fe_values[pressure].value (j, q);
		local_matrix(i,j) += (phi_i_u * phi_j_u
				      +div_phi_i_u * phi_j_p
				      +phi_i_p * div_phi_j_u)
		  * fe_values.JxW(q);
                    
		local_mass_matrix(i,j) += (-phi_i_p*phi_j_p*fe_values.JxW(q));
	      }
	  }
        
      cell->get_dof_indices (local_dof_indices);
        
      constraints.distribute_local_to_global(local_matrix,
					     local_dof_indices,
					     stiffness_matrix);

      constraints.distribute_local_to_global(local_mass_matrix,
					     local_dof_indices,
					     mass_matrix);
    }
    
  stiffness_matrix.compress (VectorOperation::add);
  mass_matrix.compress (VectorOperation::add);
    
  // Set all spurious eigenvalues to be 1.234e5
  for (unsigned int i = 0; i < dof_handler->n_dofs(); ++i)
    if (constraints.is_constrained(i))
      {
	stiffness_matrix.set(i, i, 1.234e5);
	mass_matrix.set(i, i, -1.0);
      }
    
  stiffness_matrix.compress (VectorOperation::insert);
  mass_matrix.compress (VectorOperation::insert);
}


template <int dim>
void EigenProblem<dim>::solve ()
{
  SolverControl solver_control (dof_handler->n_dofs(), 1e-9);
  SLEPcWrappers::SolverLAPACK eigensolver (solver_control);
  SLEPcWrappers::TransformationShiftInvert sinvert;
    
  eigensolver.set_which_eigenpairs (EPS_SMALLEST_MAGNITUDE);
  eigensolver.set_problem_type (EPS_GHIEP);
  // eigensolver.set_transformation(sinvert);
  eigensolver.solve (stiffness_matrix, mass_matrix,
		     eigenvalues, eigenfunctions,
		     eigenfunctions.size());
    
  for (unsigned int i=0; i<eigenfunctions.size(); ++i) {
    constraints.distribute (eigenfunctions[i]);
    eigenfunctions[i] /= eigenfunctions[i].linfty_norm ();
  }
}

template <int dim>
void EigenProblem<dim>::estimate () {
  if (eigenvalue_refinement_indices.size() == 0)
    return;

  estimated_error_per_cell = 0.0;

  if(estimator_type == "kelly") { 
    for(unsigned int i=0; i<eigenvalue_refinement_indices.size(); ++i) {
      KellyErrorEstimator<dim>::estimate (*dof_handler,
					  QGauss<dim-1>(3),
					  typename FunctionMap<dim>::type(),
					  eigenfunctions[eigenvalue_refinement_indices[i]],
					  estimated_error_per_cell);
    }
  } else if(estimator_type == "boffi") {

    FEValuesCache<dim>  cache   (StaticMappingQ1<dim>::mapping, *fe,
				 QGauss<dim>(degree+2), 
				 update_values | update_gradients | update_JxW_values,
				 QGauss<dim-1>(degree+2),
				 update_values | update_gradients | update_JxW_values);

    
    FEValuesCache<dim>  cache_neighbor   (StaticMappingQ1<dim>::mapping, *fe,
					  QGauss<dim>(degree+2), 
					  update_values | update_gradients | update_JxW_values,
					  QGauss<dim-1>(degree+2),
					  update_values | update_gradients | update_JxW_values);
    
    const FEValuesExtractors::Vector velocities (0);
    const FEValuesExtractors::Scalar pressure (dim);
    
    Vector<float> local_estimate(estimated_error_per_cell.size());
    
    for(unsigned int i=0; i<eigenvalue_refinement_indices.size(); ++i) {

      unsigned int id = eigenvalue_refinement_indices[i];
      auto & sol =  eigenfunctions[id];
      
      Tensor<1,dim> tangent;


      local_estimate = 0;
      
      unsigned int index=0;
      for(auto cell : dof_handler->active_cell_iterators() ) {
	cache.reinit(cell);

	double local_eta = 0.0;	
	cache.cache_local_solution_vector("cell", sol, local_eta);

	auto &us = cache.get_values("cell", "vel", velocities,  local_eta);
	auto &curls = cache.get_curls("cell", "vel", velocities,  local_eta);
	auto &grad_ps = cache.get_gradients("cell", "gradp", pressure, local_eta);
	auto &JxW = cache.get_JxW_values();

	double h_T = cell->diameter();
	
	for(unsigned int q=0; q<JxW.size(); ++q) {

	  auto dv = us[q]-grad_ps[q];
	  local_eta += h_T*h_T * (
				  dv*dv
				  +
				  curls[q]*curls[q]

				  ) * JxW[q];
	}

	local_estimate[index] += local_eta;
	local_eta = 0;
	
	for(unsigned int f=0; f<GeometryInfo<dim>::faces_per_cell; ++f) {	  
	  auto face = cell->face(f);
	  double h_e = face->diameter();
	  tangent = face->vertex(1)-face->vertex(0);
	  tangent /= tangent.norm();
	  
	  // The face is on the boundary:
	  if(face->at_boundary()) {
	    local_eta = 0;
	    cache.reinit(cell, f);
	    cache.cache_local_solution_vector("face", sol, local_eta);
	    
	    auto &us = cache.get_values("face", "vel", velocities,  local_eta);
	    auto &JxW = cache.get_JxW_values();

	    for(unsigned int q=0; q<JxW.size(); ++q) {
	      double tu = tangent*us[q];
	      local_eta += h_e*( tu*tu ) *JxW[q];
	    }

	    local_estimate[index] += local_eta;
	    
	  } else {
	    // Make sure everything is ok...
	    Assert (cell->neighbor(f).state() == IteratorState::valid, ExcInternalError());

	    auto neighbor = cell->neighbor(f);

	    // The face we are sitting on has children. Our neighbor
	    // is refined, and we are not. Loop over subfaces and
	    // create a subface integral on the neighbor
	    if(face->has_children()) {
	      unsigned int neighbor2=cell->neighbor_face_no(f);
	      for (unsigned int subface_no=0; subface_no<face->number_of_children(); ++subface_no)
		{
		  local_eta = 0;
		  auto neighbor_child = cell->neighbor_child_on_subface(f,subface_no);
		  Assert (!neighbor_child->has_children(), ExcInternalError());
		  
		  cache_neighbor.reinit(neighbor_child, neighbor2);
		  cache_neighbor.cache_local_solution_vector("face", sol, local_eta);

		  auto nus = cache_neighbor.get_values("face", "vel", velocities,  local_eta);

		  cache.reinit(cell, f, subface_no);
		  cache.cache_local_solution_vector("face", sol, local_eta);

		  auto us = cache.get_values("face", "vel", velocities,  local_eta);
		  auto JxW = cache.get_JxW_values();

		  for(unsigned int q=0; q<JxW.size(); ++q) {
		    auto dv = (us[q]-nus[q])*tangent;
		    local_eta += h_e*( dv*dv ) *JxW[q];
		  }
		  local_estimate[index] += local_eta;
		}
	    } else if (!cell->neighbor_is_coarser(f)) {
	      local_eta = 0;
	      unsigned int neighbor2=cell->neighbor_of_neighbor(f);
	      cache_neighbor.reinit(neighbor, neighbor2);
	      cache_neighbor.cache_local_solution_vector("face", sol, local_eta);

	      auto nus = cache_neighbor.get_values("face", "vel", velocities,  local_eta);

	      cache.reinit(cell, f);
	      cache.cache_local_solution_vector("face", sol, local_eta);

	      auto us = cache.get_values("face", "vel", velocities,  local_eta);
	      auto JxW = cache.get_JxW_values();

	      for(unsigned int q=0; q<JxW.size(); ++q) {
		auto dv = (us[q]-nus[q])*tangent;
		local_eta += h_e*( dv*dv ) *JxW[q];
	      }
		  
	      local_estimate[index] += local_eta;
	    } else {
		// Neighbor is coarser than cell
	      
		local_eta = 0;
		std::pair<unsigned int,unsigned int> neighbor_face_subface
		  = cell->neighbor_of_coarser_neighbor(f);

		Assert (neighbor_face_subface.first<GeometryInfo<dim>::faces_per_cell, ExcInternalError());
		Assert (neighbor_face_subface.second<neighbor->face(neighbor_face_subface.first)->number_of_children(),
			ExcInternalError());
		Assert (neighbor->neighbor_child_on_subface(neighbor_face_subface.first, neighbor_face_subface.second)
			== cell, ExcInternalError());

		cache_neighbor.reinit(neighbor,
				      neighbor_face_subface.first,
				      neighbor_face_subface.second);

		cache_neighbor.cache_local_solution_vector("face", sol, local_eta);
		auto nus = cache_neighbor.get_values("face", "vel", velocities,  local_eta);
	      
		cache.reinit(cell, f);
		cache.cache_local_solution_vector("face", sol, local_eta);
	      
		auto us = cache.get_values("face", "vel", velocities,  local_eta);
		auto JxW = cache.get_JxW_values();
	      
		for(unsigned int q=0; q<JxW.size(); ++q) {
		  auto dv = (us[q]-nus[q])*tangent;
		  local_eta += h_e*( dv*dv ) *JxW[q];
		}
	      
		local_estimate[index] += local_eta;
	    }
	  }
	}
	++index;
      }
      std::cout << "Estimator [" << id << "]: " << local_estimate.l1_norm() << std::endl;
      AssertDimension(index, triangulation->n_active_cells());
      AssertDimension(index, local_estimate.size());

      estimated_error_per_cell += local_estimate;
    }
  } else { // Estimator type.
    Assert(false, ExcInternalError());
  }
  std::cout << "Global estimator: " << estimated_error_per_cell.l1_norm() << std::endl;
}

template <int dim>
void EigenProblem<dim>::mark_and_refine () {
  
  if (eigenvalue_refinement_indices.size() == 0)
    {
      triangulation->refine_global (1);
    }
  else
    {
      if(mark_strategy == "number")
	GridRefinement::refine_and_coarsen_fixed_number (*triangulation,
							 estimated_error_per_cell,
							 top_fraction, bottom_fraction);
      else if(mark_strategy == "fraction")
	GridRefinement::refine_and_coarsen_fixed_fraction (*triangulation,
							   estimated_error_per_cell,
							   top_fraction, bottom_fraction);
      else if(mark_strategy == "optimize")
	GridRefinement::refine_and_coarsen_optimize (*triangulation,
						     estimated_error_per_cell);
      else
	Assert(false, ExcInternalError());
      
      triangulation->execute_coarsening_and_refinement ();
    }
}


template <int dim>
void EigenProblem<dim>::output_results (const unsigned int cycle)
{
  std::string suff = "_"+Utilities::int_to_string(cycle);
  
  data_out.prepare_data_output(*dof_handler, suff);
  for (unsigned int i=0; i<eigenfunctions.size(); ++i) {
    std::string toi=Utilities::int_to_string(i);
    data_out.add_data_vector (eigenfunctions[i], "u"+toi+",u"+toi+",p"+toi);
  }
  data_out.add_data_vector (estimated_error_per_cell, "eta");
  data_out.write_data_and_clear();
}

template <int dim>
void EigenProblem<dim>::compute_error ()
{
  std::vector<double> exact_eigenvalues;
  exact_eigenvalues.push_back(2);
  exact_eigenvalues.push_back(5);
  exact_eigenvalues.push_back(5);
  exact_eigenvalues.push_back(8);
  exact_eigenvalues.push_back(10);
  exact_eigenvalues.push_back(10);
  exact_eigenvalues.push_back(13);
  exact_eigenvalues.push_back(13);
  exact_eigenvalues.push_back(17);
  exact_eigenvalues.push_back(17);

  for(unsigned int i=0; i<eigenvalues.size(); ++i) {
    eh.custom_error([this, i, exact_eigenvalues] (const unsigned int) {
	return std::abs(eigenvalues[i]-exact_eigenvalues[i]);
      }, *dof_handler, "l"+Utilities::int_to_string(i), i==0);
  }
}


template <int dim>
void EigenProblem<dim>::run ()
{
  make_grid_fe ();
  for (unsigned int cycle=0; cycle<n_cycles; ++cycle)
    {
      std::cout<<"cycle : "<<cycle+1<<std::endl;
      setup_system ();
      assemble_system ();
      solve ();
      estimate ();
      compute_error ();
      output_results (cycle);
      
      if (cycle < n_cycles-1) {
	mark_and_refine ();
	eh.output_table(std::cout);
      }
    }
  eh.output_table(std::cout);
}


template class EigenProblem<2>;

