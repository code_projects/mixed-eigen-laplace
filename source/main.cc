/* ---------------------------------------------------------------------
 * Author: Luca Heltai
 * ---------------------------------------------------------------------
 */
#include "eigen_problem.h"

int main (int argc, char *argv[])
{
    
  Utilities::MPI::MPI_InitFinalize mpi_initialization(argc, argv, 1);
    
  EigenProblem<2> laplace_problem_2;
  ParameterAcceptor::initialize("parameters_ser.prm", "used_parameters_ser.prm");
  laplace_problem_2.run ();
    
  return 0;
}
