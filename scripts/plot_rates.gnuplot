# 1: cells
# 2: dofs
# 3: u_l0
# 4: u_l1
# 5: u_l2
# 6: u_l3
# 7: u_l4
# 8: u_l5
# 9: u_l6
# 10: u_l7
# 11: u_l8

set logscale xy
set title "Errors on Eigenvalues"
plot 'error.gpl' using 1:3  w l title '(2)', \
     'error.gpl' using 1:4  w l title '(5)', \
     'error.gpl' using 1:5  w l title '(5)', \
     'error.gpl' using 1:6  w l title '(8)', \
     'error.gpl' using 1:7  w l title '(10)', \
     'error.gpl' using 1:8  w l title '(10)', \
     'error.gpl' using 1:9  w l title '(13)', \
     'error.gpl' using 1:10 w l title '(13)', \
     'error.gpl' using 1:11 w l title '(17)'

